import 'package:flutter/material.dart';


class Alertes extends StatefulWidget {
  @override
  _AlertesState createState() => _AlertesState();
}

class _AlertesState extends State<Alertes> {
  Widget buildEventCard(bool isConfirmed,Color color,String text) {
    return Padding(
      padding: const EdgeInsets.all(8.0),
      child: GestureDetector(
        onTap: (){
        Navigator.pushReplacementNamed(context, "eventsConfirmed");
        },
        child: Container(
          height: isConfirmed ? 80 : 120,
          width: 350,
          decoration: BoxDecoration(
            color: Colors.grey[300],
            borderRadius: BorderRadius.circular(20),
          ),
          child: Padding(
            padding: const EdgeInsets.all(5.0),
            child: Column(
              children: <Widget>[
                Wrap(
                  children: <Widget>[
                    Text(
                 isConfirmed ? "" : text,
                  style: new TextStyle(
                      fontSize: 14,
                      fontFamily: "Montserrat-Medium",
                      color: Colors.grey[700]),
                ),
                  ],
                ),SizedBox(height: isConfirmed?0:15,),
                Row(
                  mainAxisAlignment: MainAxisAlignment.spaceBetween,
                  children: <Widget>[
                    buildIconCard(
                        Icons.calendar_today, Colors.white, color),
                    Column(
                      crossAxisAlignment: CrossAxisAlignment.start,
                      children: <Widget>[
                        Text(
                          "Event",
                          style: new TextStyle(
                              fontSize: 15,
                              fontFamily: "Montserrat-Medium",
                              color: Colors.grey[600]),
                        ),
                        Row(
                          children: <Widget>[
                            Icon(
                              Icons.calendar_today,
                              color: Colors.yellow[900],
                              size: 15,
                            ),
                            SizedBox(
                              width: 5,
                            ),
                            Text("2019 - 06 - 12 " + " / 2019 - 06 - 12 ",
                                style: new TextStyle(
                                    fontSize: 12,
                                    fontFamily: "Montserrat-Medium",
                                    color: Colors.grey[600])),
                          ],
                        ),
                      ],
                    ),
                    buildIconCard(Icons.arrow_forward_ios, Colors.white,
                        Colors.yellow[900])
                  ],
                ),
              ],
            ),
          ),
        ),
      ),
    );
  }

  Widget buildIconCard(IconData icon, Color iconColor, Color backGroundColor) {
    return Container(
      height: 50,
      width: 50,
      decoration: BoxDecoration(
        color: backGroundColor,
        borderRadius: BorderRadius.all(Radius.circular(40)),
      ),
      child: Icon(
        icon,
        color: iconColor,
      ),
    );
  }
  @override
  Widget build(BuildContext context) {
    return Scaffold(
       appBar: new AppBar(
        leading: IconButton(
          icon: Icon(Icons.arrow_back),
          onPressed: () {
          Navigator.pushReplacementNamed(context, "HomePage");
          },
        ),
        backgroundColor: Colors.yellow[900],
        centerTitle: true,
        title: new Text(
          "Alertes",
          style: new TextStyle(color: Colors.white),
        ),
      ),
      body: ListView(
        physics: BouncingScrollPhysics(),
        children: <Widget>[
          buildEventCard(true,Colors.pink,""),
          buildEventCard(false,Colors.pink,"Event not accode"),
          buildEventCard(false,Colors.green,"Event not accode"),
          buildEventCard(false,Colors.deepPurple,"You need To Confirme"),
          buildEventCard(false,Colors.orange,"Wataing for the reponse"),
          buildEventCard(false,Colors.orange,"Wataing for the reponse"),
          buildEventCard(false,Colors.orange,"Wataing for the reponse"),
          buildEventCard(false,Colors.orange,"Wataing for the reponse"),
        ],
      ),
    );
  }
}