import 'package:flutter/material.dart';

class Document extends StatefulWidget {
  @override
  _DocumentState createState() => _DocumentState();
}

class _DocumentState extends State<Document> {
  int _index = 0;
  bool isSelected = true;
  Widget buildEventCard(String title, IconData icon, String subtitle) {
    return Padding(
      padding: const EdgeInsets.all(8.0),
      child: Container(
        height: 55,
        width: 350,
        decoration: BoxDecoration(
          color: Colors.grey[300],
          borderRadius: BorderRadius.circular(30),
        ),
        child: Padding(
          padding: const EdgeInsets.all(5.0),
          child: Row(
            children: <Widget>[
              Container(
                height: 45,
                width: 45,
                decoration: BoxDecoration(
                  color: Colors.white,
                  borderRadius: BorderRadius.all(Radius.circular(40)),
                ),
                child: Icon(
                  Icons.alternate_email,
                  color: Colors.yellow[900],
                ),
              ),
              Padding(
                  padding: const EdgeInsets.all(8.0),
                  child: Text(
                    "Cart vitile",
                    style: new TextStyle(
                        fontSize: 16,
                        fontFamily: "Montserrat-Medium",
                        color: Colors.grey[600]),
                  )),
              Padding(
                padding: const EdgeInsets.only(left:140.0),
                child: Container(
                height: 45,
                width: 45,
                decoration: BoxDecoration(
                  color: Colors.yellow[900],
                  borderRadius: BorderRadius.all(Radius.circular(35))
                ),
                child: Icon(Icons.arrow_forward_ios,color:Colors.white),
            ),
              ),
            ],
          ),
        ),
      ),
    );
  }
  Widget buildWeek() {
    return Padding(
      padding: const EdgeInsets.only(left: 15.0, right: 18, top: 20),
      child: Container(
        height: 38,
        decoration: BoxDecoration(
            border: Border.all(
              width: 3,
              color: Colors.yellow[900],
            ),
            borderRadius: BorderRadius.all(Radius.circular(35)),
            color: Colors.white),
        child: Row(
          children: <Widget>[
            GestureDetector(
              onTap: () {
                setState(() {
                  _index = 0;
                });
              },
              child: Container(
                height: 38,
                width: 103,
                decoration: BoxDecoration(
                  color: _index == 1 || _index == 2
                      ? Colors.white
                      : Colors.yellow[900],
                  borderRadius: BorderRadius.only(
                    bottomLeft: Radius.circular(30),
                    topLeft: Radius.circular(30),
                  ),
                ),
                child: Center(
                  child: Text("Documents",
                      style: new TextStyle(
                          color: _index == 1 || _index == 2
                              ? Colors.yellow[900]
                              : Colors.white)),
                ),
              ),
            ),
            VerticalDivider(width: 0, color: Colors.yellow[900]),
            GestureDetector(
              onTap: () {
                setState(() {
                  _index = 1;
                });
              },
              child: Container(
                height: 38,
                width: 103,
                decoration: BoxDecoration(
                    color: _index == 0 || _index == 2
                        ? Colors.white
                        : Colors.yellow[900]
                    /*borderRadius: BorderRadius.only(
                  bottomRight: Radius.circular(30),
                  topRight: Radius.circular(30),
                ),*/

                    ),
                child: Center(
                  child: Text("Paie",
                      style: new TextStyle(
                          color: _index == 0 || _index == 2
                              ? Colors.yellow[900]
                              : Colors.white)),
                ),
              ),
            ),
            VerticalDivider(
              width: 05,
            ),
            GestureDetector(
              onTap: () {
                setState(() {
                  _index = 2;
                });
              },
              child: Container(
                height: 98,
                width: 110,
                decoration: BoxDecoration(
                    color: _index == 0 || _index == 1
                        ? Colors.white
                        : Colors.yellow[900],
                    borderRadius: BorderRadius.only(
                      bottomRight: Radius.circular(30),
                      topRight: Radius.circular(30),
                    )),
                child: Center(
                  child: Text("Reieves",
                      style: new TextStyle(
                          color: _index == 0 || _index == 1
                              ? Colors.yellow[900]
                              : Colors.white)),
                ),
              ),
            ),
          ],
        ),
      ),
    );
  }
  @override
  Widget build(BuildContext context) {
    return Scaffold(
      appBar: new AppBar(
        leading: IconButton(
          icon: Icon(Icons.arrow_back),
          onPressed: () {
            Navigator.pushReplacementNamed(context,"HomePage");
          },
        ),
        backgroundColor: Colors.yellow[900],
        centerTitle: true,
        title: new Text(
          "Document",
          style: new TextStyle(color: Colors.white),
        ),
      ),
      body: ListView(
        children: <Widget>[
          SizedBox(
            height: 20,
          ),
          buildWeek(),
          SizedBox(height: 20),
          buildEventCard("Place of event", Icons.location_on,
              "2019 - 06 - 12 " + " / 2019 - 06 - 12 "),
          buildEventCard(
              "Place of event", Icons.watch_later, "At 8 March 2019"),
        ],
      ),
    );
  }
}
