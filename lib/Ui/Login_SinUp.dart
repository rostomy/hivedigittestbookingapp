import 'package:flutter/cupertino.dart';
import 'package:flutter/material.dart';
enum APPSTATE { LOGIN_STATE, SIGNUP_STATE, INTRO_STATE }

class Login_SignUp extends StatefulWidget {
  @override
  _Login_SignUpState createState() => _Login_SignUpState();
}
///FormState ----- class of FormState
enum FormState {LOGIN_STATE,LAUNCH_STATE}
class _Login_SignUpState extends State<Login_SignUp> {
  FormState _formState = FormState.LAUNCH_STATE;
  ///// changing the form from LaunchState to Login_State
  void changeFormToLoginState(){
    setState(() {
     _formState = FormState.LOGIN_STATE; 
    });
  }
    ///// changing the form from LaunchState to Login_State
  void changeFormToLaunchState(){
    setState(() {
     _formState = FormState.LAUNCH_STATE;
    });
  } 
  //// buildInputTexts
  Widget buildInputs(IconData icon, String title, bool showText,Color color) {
    return Container(
      height: 55,
      width: 300,
      decoration: BoxDecoration(
        border: Border.all(
          color: color,
          width: 2,
        ),
        color: Colors.transparent,
        borderRadius: BorderRadius.all(Radius.circular(30)),
      ),
      child: Padding(
        padding: const EdgeInsets.all(8.0),
        child: TextFormField(
          obscureText: showText,
          maxLines: 3,
          decoration: InputDecoration(
            icon: Icon(
              icon,
              color:color,
            ),
            contentPadding: EdgeInsets.all(8),
            hintText: title,
            hintStyle: TextStyle(
              color: color,
              fontWeight: FontWeight.w500,
            ),
            border: InputBorder.none,
          ),
          validator: (value)=>value.isEmpty? "Can't be empty":null,
        ),
      ),
    );
  }
  ////buildTheLaunchState Ui
Widget buildLunchState(){
  return GestureDetector(
    onTap: (){
     setState(() {
      changeFormToLoginState();
     });   
    },
    child: Center(
   child: Padding(
     padding: const EdgeInsets.only(top:80.0),
     child: Text("Booking",style:TextStyle(
       color:Colors.white,fontSize:45,
       fontWeight:FontWeight.bold
     )),
   ),
  ),
  );
}
////ChangePassword
  Future<Null> showMotPass(context) {
    return showDialog(
        context: context,
        builder: (BuildContext context) {
          return SimpleDialog(
            shape: RoundedRectangleBorder(
                borderRadius: BorderRadius.all(Radius.circular(30))),
            children: <Widget>[
              Container(
                child: Column(
                  children: <Widget>[
                    Padding(
                     padding: EdgeInsets.only(top: 0,left: 60),
                      child: Row(
                        children: <Widget>[
                          Text("Mot pass oublie",style: new TextStyle(
                            color: Colors.yellow[900],fontSize: 19,fontWeight: FontWeight.bold
                          ),),
                           IconButton(
                        onPressed: (){
                          Navigator.pop(context);
                        },
                        icon: Icon(Icons.clear,color:Colors.red,size:30),
                      ),
                        ],
                      ),
                    ),
                    Text("Deemend un nouve mot de pass ",style: new TextStyle(
                      color: Colors.grey[500],
                    ),),SizedBox(height: 20,),
                    Padding(
                      padding: const EdgeInsets.only(right:15.0,left:15),
                      child: buildInputs(
                        Icons.person,
                        "votre email",
                        false,
                        Colors.grey[500],
                      ),
                    ),
                  ],
                ),
              ),
              Padding(
                padding: const EdgeInsets.only(top: 15.0, left: 60, right: 60),
                child: Container(
                  height: 50,
                  decoration: BoxDecoration(
                      borderRadius: BorderRadius.all(Radius.circular(30)),
                      color: Colors.yellow[900]),
                  child: Center(
                      child: Text("Next",
                          style: new TextStyle(
                            color: Colors.white,
                            fontSize: 16,
                            fontFamily: "Montserrat-Medium",
                          ))),
                ),
              ),
              SizedBox(height: 20),
            ],
          );
        });
  }
  /////buildLoginState Ui
    Widget buildLoginState(){
    return  Stack(
      children: <Widget>[
         Opacity(
                opacity: 0.7,
                child: ClipPath(
                  clipper: Clip(),
                  child: Container(
                    height: 250,
                    width: 400,
                    color: Colors.white,
                    child: Stack(
                      children: <Widget>[
                        Padding(
                          padding: EdgeInsets.only(top: 135, left: 95),
                          child: Text(
                            "Booking ",
                            style: TextStyle(
                                color: Colors.yellow[900],
                                fontSize: 43,
                                fontWeight: FontWeight.w800),
                          ),
                        ),
                      ],
                    ),
                  ),
                ),
              ),
              Padding(
                padding: EdgeInsets.only(top: 250, left: 25, right: 25),
                child: ListView(
                  children: <Widget>[
                    buildInputs(Icons.person, "Votre email", false,Colors.white),
                    SizedBox(
                      height: 10,
                    ),
                    buildInputs(Icons.lock, "Votre mot pass", true,Colors.white),
                    Padding(
                      padding:
                          const EdgeInsets.only(top: 15.0, left: 70, right: 70),
                      child: GestureDetector(
                        onTap: (){
                          Navigator.pushReplacementNamed(context, 'HomePage');
                        },
                        child: Container(
                          height: 50,
                          decoration: BoxDecoration(
                              borderRadius: BorderRadius.all(Radius.circular(30)),
                              color: Colors.yellow[900]),
                          child: Center(
                              child: Text("se connecter",
                                  style: new TextStyle(
                                    color: Colors.white,
                                    fontSize: 16,
                                    fontFamily: "Montserrat-Medium",
                                  ))),
                        ),
                      ),
                    ),
                    SizedBox(
                      height: 15,
                    ),
                    FlatButton(
                      child: Text(
                        "Mot pass oublie !",
                        style: new TextStyle(
                          color: Colors.white,
                        ),
                      ),
                      onPressed: () {
                        showMotPass(context);
                      },
                    ),
                    Padding(
                      padding:
                          const EdgeInsets.only(top: 10.0, left: 55, right: 55),
                      child: Container(
                        height: 50,
                        decoration: BoxDecoration(
                            borderRadius: BorderRadius.all(Radius.circular(30)),
                            color: Colors.white),
                        child: Center(
                            child: Text("Inscrivez-vous pour un compt",
                                style: new TextStyle(
                                  color: Colors.black,
                                  fontSize: 12,
                                  fontFamily: "Montserrat-Medium",
                                ))),
                      ),
                    ),
                  ],
                ),
              )
      ],
    );
  }
  @override
  Widget build(BuildContext context) {
    return Scaffold(
        body: Container(
            width: 400,
            height: 640,
            color: Colors.yellow[900],
            child: Stack(children: <Widget>[
              Opacity(
                opacity: 0.3,
                child: Container(
                  decoration: BoxDecoration(
                      image: DecorationImage(
                    fit: BoxFit.cover,
                    image: AssetImage("assets/me1.jpg"),
                  )),
                ),
              ),
              Container(
                child: buildLoginState()   /*_formState == FormState.LOGIN_STATE
                 ? : buildLunchState(),*/
                 /*Cheking whater the form is onLoginStateOrLaunchState*/ 
              ),
            ])));
  }
}
class Clip extends CustomClipper<Path> {
  @override
  Path getClip(Size size) {
    var path = Path();
    path.lineTo(0.0, size.height - 60);
    path.quadraticBezierTo(
        size.width / 4, size.height, size.width / 2, size.height);
    path.quadraticBezierTo(size.width - (size.width / 4), size.height,
        size.width, size.height / 2);
    path.lineTo(size.width, 0.0);
    path.close();
    return path;
  }

  @override
  bool shouldReclip(CustomClipper<Path> oldClipper) {
    return null;
  }
}
