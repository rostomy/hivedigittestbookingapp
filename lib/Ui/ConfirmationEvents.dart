import 'package:flutter/material.dart';


class ConfirmationEevents extends StatefulWidget {
  @override
  _ConfirmationEeventsState createState() => _ConfirmationEeventsState();
}
class _ConfirmationEeventsState extends State<ConfirmationEevents> {
   Widget buildEventCard(String title,IconData icon,String subtitle) {
    return Padding(
      padding: const EdgeInsets.all(8.0),
      child: Container(
        height:  65 ,
        width: 350,
        decoration: BoxDecoration(
          color: Colors.grey[300],
          borderRadius: BorderRadius.circular(20),
        ),
        child: Padding(
          padding: const EdgeInsets.all(5.0),
          child: Column(
            children: <Widget>[
              Row(
                children: <Widget>[
                  buildIconCard(
                      icon, Colors.yellow[900], Colors.white,),
                  Padding(
                    padding: const EdgeInsets.all(8.0),
                    child: Column(
                      crossAxisAlignment: CrossAxisAlignment.start,
                      children: <Widget>[
                        Text(
                          title,
                          style: new TextStyle(
                              fontSize: 15,
                              fontFamily: "Montserrat-Medium",
                              color: Colors.grey[600]),
                        ),
                        Row(
                          children: <Widget>[
                            Icon(
                              Icons.calendar_today,
                              color: Colors.yellow[900],
                              size: 15,
                            ),
                            SizedBox(
                              width: 5,
                            ),
                            Text(subtitle,
                                style: new TextStyle(
                                    fontSize: 12,
                                    fontFamily: "Montserrat-Medium",
                                    color: Colors.grey[600])),
                          ],
                        ),
                      ],
                    ),
                  ),
                ],
              ),
            ],
          ),
        ),
      ),
    );
  }

  Widget buildIconCard(IconData icon, Color iconColor, Color backGroundColor) {
    return Container(
      height: 50,
      width: 50,
      decoration: BoxDecoration(
        color: Colors.white,
        borderRadius: BorderRadius.all(Radius.circular(40)),
      ),
      child: Icon(
        icon,
        color: iconColor,
      ),
    );
  }
  Widget buildButton() {
    return Padding(
      padding: const EdgeInsets.all(8.0),
      child: Container(
        height: 40,
        decoration: BoxDecoration(
         color: Colors.deepPurple[500],
          borderRadius: BorderRadius.all(Radius.circular(30)),
        ),
        child: Center(
          child: Text(
            "Confirmation Finle",
            style: new TextStyle(
                color: Colors.white, fontWeight: FontWeight.w500, fontSize: 16),
          ),
        ),
      ),
    );
  }
  Widget buildDaysCard(String day,String time,IconData icon){
    return  Padding(
      padding: const EdgeInsets.all(8.0),
      child: Material(
        color: Colors.transparent,
        elevation: 10,
        child: Container(
          height:  65 ,
          width: 350,
          decoration: BoxDecoration(
            color: Colors.grey[300],
            borderRadius: BorderRadius.circular(20),
          ),
          child: Padding(
            padding: const EdgeInsets.all(5.0),
            child: Column(
              children: <Widget>[
                Row(
                 mainAxisAlignment: MainAxisAlignment.spaceBetween,
                  children: <Widget>[
                    Padding(
                      padding: const EdgeInsets.all(8.0),
                      child: Column(
                        crossAxisAlignment: CrossAxisAlignment.start,
                        children: <Widget>[
                          Text(
                            day,
                            style: new TextStyle(
                                fontSize: 15,
                                fontFamily: "Montserrat-Medium",
                                color: Colors.grey[600]),
                          ),
                          Row(
                            children: <Widget>[
                              Icon(
                                Icons.calendar_today,
                                color: Colors.yellow[900],
                                size: 15,
                              ),
                              SizedBox(
                                width: 5,
                              ),
                              Text(time,
                                  style: new TextStyle(
                                      fontSize: 12,
                                      fontFamily: "Montserrat-Medium",
                                      color: Colors.grey[600])),
                            ],
                          ),
                        ],
                      ),
                    ),
                    Container(
                  height: 50,
                  width: 100,
                  decoration: BoxDecoration(
                    color: Colors.white,
                    borderRadius: BorderRadius.all(Radius.circular(40)),
                  ),
                  child: Row(
                    mainAxisAlignment: MainAxisAlignment.spaceEvenly,
                    children: <Widget>[
                      Icon(Icons.watch_later,color:Colors.yellow[900],size:30),
                      Column(
                      mainAxisAlignment: MainAxisAlignment.center,
                        children: <Widget>[
                          Text("18:00 ",style: new TextStyle(
                            color: Colors.yellow[900],fontSize: 12
                          ),),
                          Text("22:00",style: new TextStyle(
                            color: Colors.yellow[900],fontSize: 12
                          ),),
                        ],
                      ),
                    ],
                  ),
                ),
                Container(
                  height: 45,
                  width: 45,
                  decoration: BoxDecoration(
                    color: Colors.green[400],
                    borderRadius: BorderRadius.all(Radius.circular(35))
                  ),
                  child: Icon(Icons.calendar_today,color:Colors.white),
                ),
                  ],
                ),
                
              ],
            ),
          ),
        ),
      ),
    );
      }
      Widget  buildCheckRadioButton(){
      return Container(
            height: 35,
            decoration: BoxDecoration(
              color: Colors.grey[400],
              borderRadius: BorderRadius.all(Radius.circular(30)),
            ),
            child: Padding(
              padding: const EdgeInsets.only(right:8.0,left: 8,top: 8,bottom: 8),
              child: Container(
                decoration: BoxDecoration(
                  color: Colors.green,
                  borderRadius: BorderRadius.all(Radius.circular(40)),
                  border: Border.all(
                    color: Colors.white,
                    width: 1
                  )
                ),
              ),
            ),
        );
      }

      var style = new TextStyle(
        fontSize: 13,
                fontFamily: "Montserrat-Medium",
                 color: Colors.grey[600],
               );
  @override
  Widget build(BuildContext context) {
    return Scaffold(
      appBar: new AppBar(
        leading: IconButton(
          onPressed: (){
             Navigator.pushReplacementNamed(context, "HomePage");
          },
          icon: Icon(Icons.arrow_back,color: Colors.white,),
        ),
        centerTitle: true,
        backgroundColor: Colors.yellow[900],
        title: new Text("Confirmed",style: new TextStyle(
        color: Colors.white,fontSize: 18
        ),),
      ),
      body: ListView(
        physics: BouncingScrollPhysics(),
        children: <Widget>[
          buildEventCard( "Place of event",Icons.location_on,"2019 - 06 - 12 " + " / 2019 - 06 - 12 "),
          buildEventCard(  "Place of event",Icons.watch_later,"At 8 March 2019"),
          buildEventCard( "Gere par : ",Icons.person,"Arther & Federique"),
          buildButton(),
          buildEventCard("Mission", Icons.airline_seat_flat, "Hotel White Dream"),
          buildDaysCard("SunDay", "2019 - 06 - 12  ", Icons.calendar_today),
          buildDaysCard("SunDay", "2019 - 06 - 12  ", Icons.calendar_today),
        Padding(
          padding: const EdgeInsets.all(8.0),
          child: Material(
          color: Colors.transparent,
          elevation: 10,
          child: Container(
            height:  130 ,
            width: 350,
            decoration: BoxDecoration(
              color: Colors.grey[300],
              borderRadius: BorderRadius.circular(20),
            ),
            child: Column(
              children: <Widget>[
               Text("Nous revene vere rapidment avec",style: style,),SizedBox(height: 5,),
               Text("tous le destiles de l event en ",style: style,),SizedBox(height: 5,),
               Text("complatent la feuiler de route ",style: style,),SizedBox(height: 5,),
               InkWell(
                 onTap: (){

                 },
                 child: Text("Bonn Journe",style: new TextStyle(
                   color: Colors.yellow[900],
                   fontSize: 16,
                 ),),
               ),SizedBox(height: 5,),
               InkWell(
                 onTap: (){

                 },
                 child: Text("REHUER",style: new TextStyle(
                   fontSize: 16,color: Colors.grey[500]
                 ),),
               ),
              ],
            ),)),
        ),
       Column(),
        Center(
          child: Text("Type a message Complatine",style: new TextStyle(
          color:Colors.grey[500],fontSize: 16
        ),)),
         Padding(
          padding: const EdgeInsets.all(8.0),
          child: Container(
            height:  130 ,
            width: 350,
            decoration: BoxDecoration(
              border: Border.all(
                width: 1.5,
                color: Colors.grey[400]
              ),
              color: Colors.transparent,
              borderRadius: BorderRadius.circular(20),
            ),
            child: Padding(
              padding: const EdgeInsets.all(2.0),
              child: TextField(
                maxLines: 6,
                
                decoration: InputDecoration(
                  contentPadding: EdgeInsets.all(7),
                  hintText: "Type your Message",
                  border: InputBorder.none
                ),
              ),
            ),
            )
            ),
        Padding(
          padding: const EdgeInsets.only(top:8.0,left:80,right: 80),
          child: Container(
            height: 50,
            decoration: BoxDecoration(
              borderRadius: BorderRadius.all(Radius.circular(30)),
              color: Colors.yellow[900]
            ),
            child: Center(child: Text("Send",style: new TextStyle(
              color: Colors.white,fontSize: 16,fontFamily: "Montserrat-Medium",
            ))),
          ),
        ),
        SizedBox(height: 20),
        ],
      ),
    );
  }
}