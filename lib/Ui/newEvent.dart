import 'package:flutter/material.dart';
import 'package:flutter/widgets.dart';

class NewEvent extends StatefulWidget {
  @override
  _NewEventState createState() => _NewEventState();
}

class _NewEventState extends State<NewEvent> with SingleTickerProviderStateMixin {
   bool isChecked = false;
  Duration _duration = Duration(milliseconds: 370);
  Animation<Alignment> _animation;
  AnimationController _animationController;
   void initState() {
    super.initState();
    _animationController = AnimationController(
      vsync: this,
      duration: _duration
    );
    _animation = AlignmentTween(
      begin: Alignment.centerLeft,
      end: Alignment.centerRight
    ).animate(
        CurvedAnimation(
          parent: _animationController,
          curve: Curves.bounceOut,
          reverseCurve: Curves.bounceIn
        ),
    );
  }
  @override
  void dispose() { 
     _animationController.dispose();
    super.dispose();
  }
  
  Widget buildEventCard(String title,IconData icon,String subtitle) {
    return Padding(
      padding: const EdgeInsets.all(8.0),
      child: Material(
        color: Colors.transparent,
        elevation: 10,
        child: Container(
          height:  65 ,
          width: 350,
          decoration: BoxDecoration(
            color: Colors.grey[300],
            borderRadius: BorderRadius.circular(30),
          ),
          child: Padding(
            padding: const EdgeInsets.all(5.0),
            child: Column(
              children: <Widget>[
                Row(
                  children: <Widget>[
                    buildIconCard(
                        icon, Colors.yellow[900], Colors.white,),
                    Padding(
                      padding: const EdgeInsets.all(8.0),
                      child: Column(
                        crossAxisAlignment: CrossAxisAlignment.start,
                        children: <Widget>[
                          Text(
                            title,
                            style: new TextStyle(
                                fontSize: 15,
                                fontFamily: "Montserrat-Medium",
                                color: Colors.grey[600]),
                          ),
                          Row(
                            children: <Widget>[
                              Icon(
                                Icons.calendar_today,
                                color: Colors.yellow[900],
                                size: 15,
                              ),
                              SizedBox(
                                width: 5,
                              ),
                              Text(subtitle,
                                  style: new TextStyle(
                                      fontSize: 12,
                                      fontFamily: "Montserrat-Medium",
                                      color: Colors.grey[600])),
                            ],
                          ),
                        ],
                      ),
                    ),
                  ],
                ),
              ],
            ),
          ),
        ),
      ),
    );
  }

  Widget buildIconCard(IconData icon, Color iconColor, Color backGroundColor) {
    return Container(
      height: 50,
      width: 50,
      decoration: BoxDecoration(
        color: Colors.white,
        borderRadius: BorderRadius.all(Radius.circular(40)),
      ),
      child: Icon(
        icon,
        color: iconColor,
      ),
    );
  }
    Widget buildDaysCard(String day,String time,IconData icon){
    return  Padding(
      padding: const EdgeInsets.all(8.0),
      child: Container(
        height:  65 ,
        width: 350,
        decoration: BoxDecoration(
          color: Colors.grey[300],
          borderRadius: BorderRadius.circular(30),
        ),
        child: Padding(
          padding: const EdgeInsets.all(5.0),
          child: Column(
            children: <Widget>[
              Row(
               mainAxisAlignment: MainAxisAlignment.spaceBetween,
                children: <Widget>[
                  Padding(
                    padding: const EdgeInsets.all(8.0),
                    child: Column(
                      crossAxisAlignment: CrossAxisAlignment.start,
                      children: <Widget>[
                        Text(
                          day,
                          style: new TextStyle(
                              fontSize: 15,
                              fontFamily: "Montserrat-Medium",
                              color: Colors.grey[600]),
                        ),
                        Row(
                          children: <Widget>[
                            Icon(
                              Icons.calendar_today,
                              color: Colors.yellow[900],
                              size: 15,
                            ),
                            SizedBox(
                              width: 5,
                            ),
                            Text(time,
                                style: new TextStyle(
                                    fontSize: 12,
                                    fontFamily: "Montserrat-Medium",
                                    color: Colors.grey[600])),
                          ],
                        ),
                      ],
                    ),
                  ),
                  Container(
                height: 50,
                width: 100,
                decoration: BoxDecoration(
                  color: Colors.white,
                  borderRadius: BorderRadius.all(Radius.circular(40)),
                ),
                child: Row(
                  mainAxisAlignment: MainAxisAlignment.spaceEvenly,
                  children: <Widget>[
                    Icon(Icons.watch_later,color:Colors.yellow[900],size:30),
                    Column(
                    mainAxisAlignment: MainAxisAlignment.center,
                      children: <Widget>[
                        Text("18:00 ",style: new TextStyle(
                          color: Colors.yellow[900],fontSize: 12
                        ),),
                        Text("22:00",style: new TextStyle(
                          color: Colors.yellow[900],fontSize: 12
                        ),),
                      ],
                    ),
                  ],
                ),
              ),
              AnimatedBuilder(
      animation: _animationController,
      builder: (context, child){
      return Container(
        width: 70,
        height: 40,
        padding: EdgeInsets.fromLTRB(0, 6, 0, 6),
        decoration: BoxDecoration(
          color: isChecked ? Colors.green : Colors.grey,
          borderRadius: BorderRadius.all(
            Radius.circular(40),
          ),
          boxShadow: [
            BoxShadow(
              color: isChecked ? Colors.green : Colors.grey,
              blurRadius: 12,
              offset: Offset(0, 8)
            )
          ]
        ),
        child: Stack(
          children: <Widget>[
            Align(
              alignment: _animation.value,
              child: GestureDetector(
              onTap: (){
                setState(() {
                  if(_animationController.isCompleted){
                    _animationController.reverse();
                  }else{
                    _animationController.forward();
                  }

                  isChecked = !isChecked;
                });
              },
              child: Container(
                width: 50,
                height: 50,
                decoration: BoxDecoration(
                  shape: BoxShape.circle,
                  color: Colors.white,
                ),
              ),
              ),
            )
          ],
        ),
      );
      },
    ),
                ],
              ),
              
            ],
          ),
        ),
      ),
    );
      }
  @override

 
  Widget build(BuildContext context) {
    return Scaffold(
      appBar: new AppBar(
        leading: IconButton(
          onPressed: (){

          },
          icon: Icon(Icons.arrow_back,color: Colors.white,),
        ),
        centerTitle: true,
        backgroundColor: Colors.yellow[900],
        title: new Text("Avertissments",style: new TextStyle(
        color: Colors.white,fontSize: 18
        ),),
      ),
      body: ListView(
        children: <Widget>[
          buildEventCard( "Place of event",Icons.location_on,"2019 - 06 - 12 " + " / 2019 - 06 - 12 "),
          buildEventCard(  "Place of event",Icons.watch_later,"At 8 March 2019"),
          buildEventCard( "Gere par : ",Icons.person,"Arther & Federique"),
           Padding(
          padding: const EdgeInsets.all(8.0),
          child: Container(
            height:  100,
            width: 350,
            decoration: BoxDecoration(
              color: Colors.green[500],
              borderRadius: BorderRadius.circular(30),
            ),
            child: 
               Padding(
                 padding: const EdgeInsets.only(top:25.0,left:29,right: 15),
                 child: Text("Bonjour  Mathilde,soultha tu partcipt a cet evelment sur un ou pluisir des mision disponilbed",style: TextStyle(
                   color: Colors.white,
                 ),),
               )
            ),
        ),
          buildDaysCard("SunDay", "2019 - 06 - 12  ", Icons.calendar_today),
          buildDaysCard("SunDay", "2019 - 06 - 12  ", Icons.calendar_today),
          buildDaysCard("SunDay", "2019 - 06 - 12  ", Icons.calendar_today),
           Padding(
          padding: const EdgeInsets.all(8.0),
          child: Container(
            height:  130 ,
            width: 350,
            decoration: BoxDecoration(
              border: Border.all(
                width: 1.5,
                color: Colors.grey[400]
              ),
              color: Colors.transparent,
              borderRadius: BorderRadius.circular(20),
            ),
            child: Padding(
              padding: const EdgeInsets.all(2.0),
              child: TextField(
                maxLines: 6,
                decoration: InputDecoration(
                  contentPadding: const EdgeInsets.all(5.0),
                  hintText :"Type a message her",
                  border: InputBorder.none
                ),
              ),
            ),
            )
            ),
          Padding(
          padding: const EdgeInsets.only(top:8.0,left:80,right: 80),
          child: Container(
            height: 50,
            decoration: BoxDecoration(
              borderRadius: BorderRadius.all(Radius.circular(30)),
              color: Colors.yellow[900]
            ),
            child: Center(child: Text("Send",style: new TextStyle(
              color: Colors.white,fontSize: 16,fontFamily: "Montserrat-Medium",
            ))),
          ),
        ),
        SizedBox(height: 30,),
        ],
      ),
    );
  }
}