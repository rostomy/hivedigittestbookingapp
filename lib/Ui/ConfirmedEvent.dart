import 'package:flutter/cupertino.dart' as prefix0;
import 'package:flutter/material.dart';
import 'package:flutter/rendering.dart';

class ConfirmedEvents extends StatefulWidget {
  @override
  _ConfirmedEventsState createState() => _ConfirmedEventsState();
}

class _ConfirmedEventsState extends State<ConfirmedEvents> {
  int _index = 0;
  bool isSelected = true;
  final PageController _pageController = PageController();
  void initState(){
    super.initState();
    
  }
  Widget buildWeek() {
    return Padding(
      padding: const EdgeInsets.only(left: 15.0, right: 18, top: 20),
      child: Container(
        height: 38,
        decoration: BoxDecoration(
            border: Border.all(
              width: 3,
              color: Colors.yellow[900],
            ),
            borderRadius: BorderRadius.all(Radius.circular(35)),
            color: Colors.white),
        child: Row(
          children: <Widget>[
            GestureDetector(
              onTap: () {
                setState(() {
                  _index = 0;
                });
              },
              child: Container(
                height: 38,
                width: 103,
                decoration: BoxDecoration(
                  color: _index == 1 || _index == 2
                      ? Colors.white
                      : Colors.yellow[900],
                  borderRadius: BorderRadius.only(
                    bottomLeft: Radius.circular(30),
                    topLeft: Radius.circular(30),
                  ),
                ),
                child: Center(
                  child: Text("Mois",
                      style: new TextStyle(
                          color: _index == 1 || _index == 2
                              ? Colors.yellow[900]
                              : Colors.white)),
                ),
              ),
            ),
            VerticalDivider(width: 0, color: Colors.yellow[900]),
            GestureDetector(
              onTap: () {
                setState(() {
                  _index = 1;
                });
              },
              child: Container(
                height: 38,
                width: 103,
                decoration: BoxDecoration(
                    color: _index == 0 || _index == 2
                        ? Colors.white
                        : Colors.yellow[900]
                    /*borderRadius: BorderRadius.only(
                  bottomRight: Radius.circular(30),
                  topRight: Radius.circular(30),
                ),*/

                    ),
                child: Center(
                  child: Text("semine",
                      style: new TextStyle(
                          color: _index == 0 || _index == 2
                              ? Colors.yellow[900]
                              : Colors.white)),
                ),
              ),
            ),
            VerticalDivider(
              width: 05,
            ),
            GestureDetector(
              onTap: () {
                setState(() {
                  _index = 2;
                });
              },
              child: Container(
                height: 98,
                width: 110,
                decoration: BoxDecoration(
                    color: _index == 0 || _index == 1
                        ? Colors.white
                        : Colors.yellow[900],
                    borderRadius: BorderRadius.only(
                      bottomRight: Radius.circular(30),
                      topRight: Radius.circular(30),
                    )),
                child: Center(
                  child: Text("List",
                      style: new TextStyle(
                          color: _index == 0 || _index == 1
                              ? Colors.yellow[900]
                              : Colors.white)),
                ),
              ),
            ),
          ],
        ),
      ),
    );
  }

  Widget buildDay() {
    return Padding(
        padding: const EdgeInsets.only(left: 10.0, right: 10),
        child: Container(
          height: 35,
          width: 350,
          decoration: BoxDecoration(
            color: Colors.grey[300],
            borderRadius: BorderRadius.circular(20),
          ),
          child: Row(
            mainAxisAlignment: MainAxisAlignment.spaceAround,
            children: <Widget>[
              Text(
                "17 - 12 - 2019",
                style: new TextStyle(
                    fontSize: 14,
                    fontFamily: "Montserrat-Medium",
                    color: Colors.grey[600]),
              ),
              Text(
                "Lunday",
                style: new TextStyle(
                    fontSize: 14,
                    fontFamily: "Montserrat-Medium",
                    color: Colors.grey[600]),
              ),
            ],
          ),
        ));
  }

  Widget buildDaysCard(String day, String time, IconData icon) {
    return Padding(
      padding: const EdgeInsets.all(8.0),
      child: Container(
        height: 65,
        width: 350,
        decoration: BoxDecoration(
          color: Colors.grey[300],
          borderRadius: BorderRadius.circular(20),
        ),
        child: Padding(
          padding: const EdgeInsets.all(5.0),
          child: Column(
            children: <Widget>[
              Row(
                mainAxisAlignment: MainAxisAlignment.spaceEvenly,
                children: <Widget>[
                  Container(
                    height: 50,
                    width: 100,
                    decoration: BoxDecoration(
                      color: Colors.white,
                      borderRadius: BorderRadius.all(Radius.circular(40)),
                    ),
                    child: Row(
                      mainAxisAlignment: MainAxisAlignment.spaceEvenly,
                      children: <Widget>[
                        Icon(Icons.watch_later,
                            color: Colors.yellow[900], size: 30),
                        Column(
                          mainAxisAlignment: MainAxisAlignment.center,
                          children: <Widget>[
                            Text(
                              "18:00 ",
                              style: new TextStyle(
                                  color: Colors.yellow[900], fontSize: 12),
                            ),
                            Text(
                              "22:00",
                              style: new TextStyle(
                                  color: Colors.yellow[900], fontSize: 12),
                            ),
                          ],
                        ),
                      ],
                    ),
                  ),
                  Padding(
                    padding: const EdgeInsets.all(8.0),
                    child: Column(
                      crossAxisAlignment: CrossAxisAlignment.start,
                      children: <Widget>[
                        Text(
                          day,
                          style: new TextStyle(
                              fontSize: 15,
                              fontFamily: "Montserrat-Medium",
                              color: Colors.grey[600]),
                        ),
                        Row(
                          children: <Widget>[
                            Icon(
                              Icons.location_on,
                              color: Colors.yellow[900],
                              size: 15,
                            ),
                            SizedBox(
                              width: 5,
                            ),
                            Text(time,
                                style: new TextStyle(
                                    fontSize: 12,
                                    fontFamily: "Montserrat-Medium",
                                    color: Colors.grey[600])),
                          ],
                        ),
                      ],
                    ),
                  ),
                  Container(
                    height: 45,
                    width: 45,
                    decoration: BoxDecoration(
                        color: Colors.yellow[900],
                        borderRadius: BorderRadius.all(Radius.circular(35))),
                    child: Icon(Icons.arrow_forward_ios, color: Colors.white),
                  ),
                ],
              ),
            ],
          ),
        ),
      ),
    );
  }

  Widget buildListEvents() {
    return Padding(
        padding: const EdgeInsets.only(right: 08.0, left: 08, bottom: 150),
        child: Container(
          height: 280,
          width: 350,
          decoration: BoxDecoration(
            color: Colors.grey[300],
            borderRadius: BorderRadius.circular(30),
          ),
          child: ListView(
            children: <Widget>[
              SizedBox(height: 10),
              Center(
                  child: Text("EVENT CONFIRMED",
                      style: new TextStyle(
                          fontSize: 12,
                          fontFamily: "Montserrat-Medium",
                          color: Colors.grey[600]))),
              buildEventCard(true, Colors.green, ""),
              buildEventCard(true, Colors.green, ""),
              buildEventCard(true, Colors.green, ""),
            ],
          ),
        ));
  }

  Widget buildEventCard(bool isConfirmed, Color color, String text) {
    return Padding(
      padding: const EdgeInsets.all(5.0),
      child: Column(
        children: <Widget>[
          Wrap(
            children: <Widget>[
              Text(
                isConfirmed ? "" : text,
                style: new TextStyle(
                    fontSize: 14,
                    fontFamily: "Montserrat-Medium",
                    color: Colors.grey[700]),
              ),
            ],
          ),
          SizedBox(
            height: isConfirmed ? 0 : 15,
          ),
          Row(
            mainAxisAlignment: MainAxisAlignment.spaceBetween,
            children: <Widget>[
              buildIconCard(Icons.calendar_today, Colors.white, color),
              Column(
                crossAxisAlignment: CrossAxisAlignment.start,
                children: <Widget>[
                  Text(
                    "Event",
                    style: new TextStyle(
                        fontSize: 15,
                        fontFamily: "Montserrat-Medium",
                        color: Colors.grey[600]),
                  ),
                  Row(
                    children: <Widget>[
                      Icon(
                        Icons.calendar_today,
                        color: Colors.yellow[900],
                        size: 15,
                      ),
                      SizedBox(
                        width: 5,
                      ),
                      Text("2019 - 06 - 12 " + " / 2019 - 06 - 12 ",
                          style: new TextStyle(
                              fontSize: 12,
                              fontFamily: "Montserrat-Medium",
                              color: Colors.grey[600])),
                    ],
                  ),
                ],
              ),
              buildIconCard(
                  Icons.arrow_forward_ios, Colors.white, Colors.yellow[900])
            ],
          ),
        ],
      ),
    );
  }

  Widget buildIconCard(IconData icon, Color iconColor, Color backGroundColor) {
    return Container(
      height: 50,
      width: 50,
      decoration: BoxDecoration(
        color: backGroundColor,
        borderRadius: BorderRadius.all(Radius.circular(40)),
      ),
      child: Icon(
        icon,
        color: iconColor,
      ),
    );
  }

  @override
  Widget build(BuildContext context) {
    return Scaffold(
        appBar: new AppBar(
          leading: IconButton(
            icon: Icon(
              Icons.arrow_back,
              color: Colors.white,
            ),
            onPressed: () {
              Navigator.pushReplacementNamed(context, "HomePage");
            },
          ),
          centerTitle: true,
          backgroundColor: Colors.yellow[900],
          title: new Text(
            "Confirmes",
            style: new TextStyle(color: Colors.white, fontSize: 18),
          ),
        ),
        body: ListView(
          primary: true,
          children: <Widget>[
            SizedBox(
              height: 20,
            ),
            buildWeek(),
            SizedBox(
              height: 20,
            ),
            Column(
              children: <Widget>[
                Container(
                    height: 500,
                    child: PageView(
                      controller: _pageController,
                      children: <Widget>[
                        ListView(
                          primary: false,
                          children: <Widget>[
                            buildDaysCard("Eventment", "Ghardia - Eletueff ",
                                Icons.calendar_today),
                            buildDaysCard("Eventment", "Ghardia - Eletueff ",
                                Icons.calendar_today),
                            buildDay(),
                            buildDaysCard("Eventment", "Ghardia - Eletueff ",
                                Icons.calendar_today),
                            buildDaysCard("Eventment", "Ghardia - Eletueff ",
                                Icons.calendar_today),
                            buildDaysCard("Eventment", "Ghardia - Eletueff ",
                                Icons.calendar_today),
                            buildDaysCard("Eventment", "Ghardia - Eletueff ",
                                Icons.calendar_today),
                            buildDaysCard("Eventment", "Ghardia - Eletueff ",
                                Icons.calendar_today),
                                SizedBox(height: 20,),
                          ],
                        ),
                        buildListEvents(),
                      ],
                    )),
              ],
            ),
          ],
        ));
  }
}
