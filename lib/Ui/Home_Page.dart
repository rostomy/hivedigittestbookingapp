import 'package:flutter/cupertino.dart';
import 'package:flutter/material.dart';
import 'package:flutter/painting.dart';

class HomePage extends StatefulWidget {
  @override
  _HomePageState createState() => _HomePageState();
}
class _HomePageState extends State<HomePage> with SingleTickerProviderStateMixin{
 static const Color color = Colors.grey;
 bool isSelected = false;
 ////Making a list of HomeCompent
 final List  <HomePageCompent> homePageCompent  = [
    HomePageCompent(
         "Alertes", Icons.notifications,  Colors.yellow[900],Colors.white,"Alertes"),
    HomePageCompent(
      "Mes Reponses ",Icons.chat, color,color,"MesReponse"),
    HomePageCompent(
        "Mes Missions",Icons.calendar_today, color,color,"MesMissions"),
    HomePageCompent(
         "Notification", Icons.notifications, color,color,"Notification"),
    HomePageCompent(
        "Profile",  Icons.person,  color,color,"Profile"),
    HomePageCompent(
         "Urgences", Icons.notifications, color,color,"Urgences"),
  ];
  /////Making a list of Menu Item 
   final List<MenuItems> items = [
     MenuItems("Home",Icons.home,Colors.yellow[900]),
     MenuItems("Notifcation",Icons.notifications,color),
     MenuItems("My Reponse",Icons.chat,color),
     MenuItems("Mission",Icons.calendar_today,color),
     MenuItems("Documents",Icons.home,color),
     MenuItems("Profile",Icons.person,color),
     MenuItems("Notfication",Icons.notifications,color),
   ];
   /////building MenuItem Ui
  Widget buildListTileMenu(MenuItems menuItem,) {
    return ListTile(
            title: Text(menuItem.title,style: new TextStyle(
                   color: menuItem.color,fontSize: 16,
                 ),),
                 leading: IconButton(
                   icon: Icon(menuItem.icon,color: menuItem.color,size: 30,),
                  onPressed: (){
                  },
                 ),
               );
  }
 @override
 void initState() { 
   super.initState();
 }
  Widget _buildIconsPage(HomePageCompent homePageCompent,) {
    return Padding(
      padding: const EdgeInsets.all(8.0),
      child: GestureDetector(
        onTap: (){
          Navigator.pushReplacementNamed(context,homePageCompent.nav);
        },
        child: Container(
          decoration: BoxDecoration(
              border: Border.all(width: 0.5, color: Colors.grey),
              gradient: LinearGradient(
                colors: [Colors.white, Colors.grey[300]],
              ),
              borderRadius: BorderRadius.all(Radius.circular(30))),
          child: Column(
            children: <Widget>[
              SizedBox(
                height: 25,
              ),
              Icon(
                homePageCompent.icon,
                color: homePageCompent.color,
                size: 46,
              ),
              SizedBox(
                height: 10,
              ),
              Text(
                homePageCompent.title,
                style: new TextStyle(
                  fontFamily: "Montserrat-Bold",
                  fontWeight: FontWeight.w100,
                  fontSize: 13,
                  color: homePageCompent.color,
                ),
              )
            ],
          ),
        ),
      ),
    );
  }
  Widget buildButton() {
    return Container(
      width: 230,
      height: 50,
      decoration: BoxDecoration(
        gradient: LinearGradient(colors: [
          Colors.yellow[900],
          Colors.yellow[800],
        ]),
        borderRadius: BorderRadius.all(Radius.circular(30)),
      ),
      child: Center(
        child: Text(
          "Connect",
          style: new TextStyle(
              color: Colors.white, fontWeight: FontWeight.w500, fontSize: 17),
        ),
      ),
    );
  }

  @override
  Widget build(BuildContext context) {
    return Scaffold(
      drawer: Drawer(
        child: ListView(
          children: <Widget>[
            ClipPath(
              clipper: Clip(),
              child: Container(
                width: 400,
                height: 200,
                decoration: BoxDecoration(
                  borderRadius: BorderRadius.only(
                      bottomLeft: Radius.circular(55),
                      bottomRight: Radius.circular(50)),
                  color: Colors.yellow[900],
                ),
                child: Stack(
                  children: <Widget>[
                    Opacity(
                      opacity: 0.3,
                      child: Container(
                        decoration: BoxDecoration(
                            image: DecorationImage(
                          fit: BoxFit.cover,
                          image: AssetImage("assets/me1.jpg"),
                        )),
                      ),
                    ),
                    Padding(
                      padding: EdgeInsets.only(top: 35, left: 100),
                      child: Container(
                        width: 100,
                        height: 100,
                        decoration: BoxDecoration(
                          border: Border.all(
                            width: 2,
                            color: Colors.white,
                          ),
                          borderRadius: BorderRadius.all(Radius.circular(50)),
                          image: DecorationImage(
                            fit: BoxFit.fill,
                            image: AssetImage("assets/me1.jpg"),
                          ),
                        ),
                      ),
                    ),
                    Padding(
                      padding: EdgeInsets.only(top: 150, left: 70),
                      child: Text(
                        "Rostomy Hadjsaid",
                        style: new TextStyle(
                            color: Colors.white,
                            fontWeight: FontWeight.w600,
                            fontSize: 17),
                      ),
                    ),
                  ],
                ),
              ),
            ),
             Padding(
               padding: EdgeInsets.only(top: 15,left:30),
               child: ListView(           
                 shrinkWrap: true,
                 children: items.map(buildListTileMenu)
                 .toList()
                 /* showingTheMenu Item On ListView*/
               )
             ),
          ],
        ),
      ),
      appBar: new AppBar(
        backgroundColor: Colors.yellow[900],
        title: new Text(
          "Accuell",
          style:
              new TextStyle(color: Colors.white, fontWeight: FontWeight.w500),
        ),
        centerTitle: true,
      ),
      body: Padding(
        padding: const EdgeInsets.only(top:13.0,left: 35,right: 35),
        child: Column(
      children: <Widget>[
        GridView.count(
          shrinkWrap: true,
          crossAxisCount: 2,
        children: homePageCompent.map(_buildIconsPage)
        .toList(),/*Showing the HomeCompentItem On GridView*/
        ),
        SizedBox(height: 4,),
        buildButton()
      ],
        ),
      )
    );
  }
}

class Clip extends CustomClipper<Path> {
  @override
  Path getClip(Size size) {
    var path = Path();
    path.lineTo(0.0, size.height - 40);
    path.quadraticBezierTo(
        size.width / 4, size.height, size.width / 2, size.height);
    path.quadraticBezierTo(size.width - (size.width / 4), size.height,
        size.width, size.height / 2);
    path.lineTo(size.width, 0.0);
    path.close();
    return path;
  }
  @override
  bool shouldReclip(CustomClipper<Path> oldClipper) {
    return null;
  }
}
class MenuItems {
  String title;
  IconData icon;
  Color color;
  MenuItems(this.title,this.icon,this.color);
}
class HomePageCompent {
   String title;
   IconData icon;
   Color color;
  Color textColor;
  String nav;
  HomePageCompent(this.title, this.icon, this.color,this.textColor,this.nav);
}
